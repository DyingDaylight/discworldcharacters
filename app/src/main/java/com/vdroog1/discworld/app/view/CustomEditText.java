package com.vdroog1.discworld.app.view;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.EditText;
import com.vdroog1.discworld.app.R;

/**
 * Created by kettricken on 09.05.2015.
 */
public class CustomEditText extends EditText {

    public CustomEditText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public CustomEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CustomEditText(Context context) {
        super(context);
        init();
    }

    public void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "font/BenguiatRegular.ttf");
        setTypeface(tf ,1);
        setTextColor(getResources().getColor(R.color.text_color));
    }

}
