package com.vdroog1.discworld.app;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.vdroog1.discworld.app.data.Character;
import com.vdroog1.discworld.app.data.DataManager;
import com.vdroog1.discworld.app.data.Player;

/**
 * Created by kettricken on 09.05.2015.
 */
public class FragmentGame extends Fragment implements DialogPassword.OnPasswordListener,
    DialogConfirmation.OnConfirmedListener
{

    public static String TAG = FragmentGame.class.getSimpleName();

    View rootView;
    RadioGroup radioGroup;
    ToggleButton finishGameBtn;
    GridView gridView;
    DataManager dataManager;

    Player currentPlayer;

    public FragmentGame() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        dataManager = DataManager.getInstance(getActivity().getBaseContext());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_game, container, false);

        gridView = (GridView) rootView.findViewById(R.id.game_grid);
        gridView.setAdapter(new PlayerAdapter(getActivity(), dataManager.getPlayers()));
        gridView.setOnItemClickListener(onCardClickListener);

        radioGroup = (RadioGroup) rootView.findViewById(R.id.game_options);
        finishGameBtn = (ToggleButton) rootView.findViewById(R.id.game_show_all);

        finishGameBtn.setOnCheckedChangeListener(onGameFinishedListener);

        return rootView;
    }

    CompoundButton.OnCheckedChangeListener onGameFinishedListener = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            if (isChecked) {
                DialogConfirmation dialogConfirmation = new DialogConfirmation();
                dialogConfirmation.setTitle(getString(R.string.game_finish));
                dialogConfirmation.setMessage(getString(R.string.game_finish_msg));
                dialogConfirmation.show(getActivity().getSupportFragmentManager(), DialogConfirmation.TAG);
            } else {
                MainActivity mainActivity = (MainActivity) getActivity();
                mainActivity.restart();
            }
        }
    };

    AdapterView.OnItemClickListener onCardClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            if (!finishGameBtn.isChecked()) {
                currentPlayer = dataManager.getPlayers().get(position);
                DialogPassword dialogPassword = new DialogPassword();
                dialogPassword.setPassword(currentPlayer.getPassword());
                dialogPassword.show(getActivity().getSupportFragmentManager(), DialogPassword.TAG);
            }
        }
    };

    @Override
    public void onOk() {
        switch (radioGroup.getCheckedRadioButtonId()) {
            case R.id.game_show_me:
                Bundle bundle = new Bundle();
                bundle.putInt(Player.playerId, currentPlayer.getId());
                bundle.putInt(Character.characterId, currentPlayer.getCharacter().getId());
                bundle.putSerializable(FragmentCharacter.STATE, FragmentCharacter.State.WATCH);
                FragmentCharacter fragmentCharacter = new FragmentCharacter();
                fragmentCharacter.setArguments(bundle);
                MainActivity mainActivity = (MainActivity) getActivity();
                mainActivity.changeFragment(fragmentCharacter, FragmentCharacter.TAG, true);
                break;
            case R.id.game_mrs_cake:
                bundle = new Bundle();
                bundle.putSerializable(FragmentCharacter.STATE, FragmentCharacterList.State.WATCH);
                FragmentCharacterList fragmentCharacterList = new FragmentCharacterList();
                fragmentCharacterList.setArguments(bundle);
                mainActivity = (MainActivity) getActivity();
                mainActivity.changeFragment(fragmentCharacterList, FragmentCharacterList.TAG, true);
                break;
            case R.id.game_zorgo:
                bundle = new Bundle();
                bundle.putInt(Player.playerId, currentPlayer.getId());
                bundle.putSerializable(FragmentCharacter.STATE, FragmentCharacterList.State.RECHOOSE);
                fragmentCharacterList = new FragmentCharacterList();
                fragmentCharacterList.setArguments(bundle);
                mainActivity = (MainActivity) getActivity();
                mainActivity.changeFragment(fragmentCharacterList, FragmentCharacterList.TAG, true);
                break;
            default:
                Toast.makeText(getActivity(), getString(R.string.game_options_empty), Toast.LENGTH_SHORT).show();
        }
        radioGroup.clearCheck();
    }

    @Override
    public void onCancel() {
        radioGroup.clearCheck();
    }

    @Override
    public void onConfirmed() {
        ((PlayerAdapter) gridView.getAdapter()).setGameFinished();
        ((PlayerAdapter) gridView.getAdapter()).notifyDataSetChanged();
        radioGroup.setVisibility(View.GONE);
    }

    @Override
    public void onConfirmationCanceled() {
        finishGameBtn.setOnCheckedChangeListener(null);
        finishGameBtn.setChecked(false);
        finishGameBtn.setOnCheckedChangeListener(onGameFinishedListener);
    }
}
