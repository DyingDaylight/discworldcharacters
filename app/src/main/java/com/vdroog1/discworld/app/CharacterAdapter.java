package com.vdroog1.discworld.app;

import android.content.Context;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import com.vdroog1.discworld.app.data.Character;

import java.util.List;
import java.util.Random;

/**
 * Created by kettricken on 09.05.2015.
 */
public class CharacterAdapter extends BaseAdapter {

    private Context context;

    private List<Character> characters;

    private float cardRatio = 0.64f;
    private boolean showCharacters = false;
    private int excludedPosition;

    public CharacterAdapter(Context context, List<Character> characters) {
        this.context = context;
        this.characters = characters;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ImageView i = new ImageView(context);

        if (showCharacters && position != excludedPosition) {
            Character character = characters.get(position);
            i.setImageResource(character.getCardId());
        } else {
            i.setImageDrawable(context.getResources().getDrawable(R.drawable.back));
        }
        i.setScaleType(ImageView.ScaleType.FIT_XY);

        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        float pxCardWidth = displayMetrics.widthPixels / 3;
        float pxCardHeight = pxCardWidth / cardRatio;

        i.setLayoutParams(new GridView.LayoutParams((int) pxCardWidth, (int) pxCardHeight));
        return i;
    }


    public final int getCount() {
        return characters.size();
    }

    public final Object getItem(int position) {
        return characters.get(position % characters.size());
    }

    public final long getItemId(int position) {
        return position;
    }

    public void setShowCharacters(boolean showCharacters) {
        this.showCharacters = showCharacters;
        if (showCharacters) {
            Random r = new Random();
            int min = 0;
            int max = characters.size();
            excludedPosition = r.nextInt(max - min) + min;
        }
    }

    public int getExcludedPosition() {
        return excludedPosition;
    }
}
