package com.vdroog1.discworld.app;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.vdroog1.discworld.app.data.Character;
import com.vdroog1.discworld.app.data.DataManager;
import com.vdroog1.discworld.app.data.Player;

/**
 * Created by kettricken on 09.05.2015.
 */
public class FragmentCharacter extends Fragment {

    public static String TAG = FragmentCharacter.class.getSimpleName();
    public static String STATE = "STATE";

    enum State { CHOOSE, WATCH, RECHOOSE }

    View rootView;

    Character character;
    Player player;
    DataManager dataManager;

    State state;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        dataManager = DataManager.getInstance(getActivity().getBaseContext());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_character, container, false);

        state = (State) getArguments().getSerializable(FragmentCharacter.STATE);

        int characterId = getArguments().getInt(Character.characterId);
        int playerId = getArguments().getInt(Player.playerId);

        Log.d("Test", "characterId " + characterId);

        character = dataManager.getCharacterById(characterId);
        player = dataManager.getPlayerById(playerId);

        Log.d("Test", "character " + character);

        TextView titleET = (TextView) rootView.findViewById(R.id.character_title);
        TextView nameET = (TextView) rootView.findViewById(R.id.character_name);
        final ImageView cardIV = (ImageView) rootView.findViewById(R.id.character_card);

        String titleStr = getResources().getString(R.string.character_title, player.getName());

        Log.d("Test", "nameET " + nameET);

        titleET.setText(titleStr);
        nameET.setText(character.getName());

        cardIV.post(new Runnable() {
            @Override
            public void run() {
                int width = cardIV.getWidth();
                int height = (int) (width / 0.64f);
                RelativeLayout.LayoutParams rlp = new RelativeLayout.LayoutParams(width, height);
                rlp.addRule(RelativeLayout.BELOW, R.id.character_name);
                rlp.addRule(RelativeLayout.CENTER_HORIZONTAL, 1);
                cardIV.setLayoutParams(rlp);
                cardIV.setImageResource(character.getCardId());
            }
        });

        rootView.findViewById(R.id.character_next_btn).setOnClickListener(onClickListener);
        rootView.findViewById(R.id.character_card).setOnClickListener(onClickListener);

        return rootView;
    }

    View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.character_card:
                    DialogDescription dialogDescription = new DialogDescription();
                    dialogDescription.setDescription(character.getDescription());
                    dialogDescription.show(getActivity().getSupportFragmentManager(), DialogDescription.TAG);
                    break;
                case R.id.character_next_btn:
                    continueToNextFragment();
                    break;
            }
        }
    };

    private void continueToNextFragment() {
        if (state == State.CHOOSE) {
            openPlayerFragment();
        } else if (state == State.WATCH) {
            getActivity().onBackPressed();
        } else if (state == State.RECHOOSE) {
            rechoosePlayer();
        }
    }

    private void rechoosePlayer() {
        dataManager.addCharacter(player.getCharacter());
        player.setCharacter(character);
        dataManager.removeCharacter(character);

        Fragment fragment = new FragmentGame();
        MainActivity mainActivity = (MainActivity) getActivity();
        mainActivity.changeFragment(fragment, FragmentGame.TAG);
    }

    private void openPlayerFragment() {
        player.setCharacter(character);
        dataManager.removeCharacter(character);

        MainActivity mainActivity = (MainActivity) getActivity();
        Fragment fragment = new FragmentPlayers();
        mainActivity.changeFragment(fragment, FragmentPlayers.TAG);
    }

    public boolean onBackPressed() {
        if (state == State.RECHOOSE) {
            Fragment fragment = new FragmentGame();
            MainActivity mainActivity = (MainActivity) getActivity();
            mainActivity.changeFragment(fragment, FragmentGame.TAG);
            return true;
        }
        return false;
    }
}
