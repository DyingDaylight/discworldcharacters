package com.vdroog1.discworld.app.data;

import android.content.Context;
import android.content.res.TypedArray;
import com.vdroog1.discworld.app.R;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

/**
 * Created by kettricken on 09.05.2015.
 */
public class DataManager {

    private static DataManager instance;

    private List<Player> players = new ArrayList<Player>();
    private List<Character> characters = new ArrayList<Character>();

    public static boolean twoPlayerGame = false;

    private DataManager(Context context) {
    }

    public static DataManager getInstance(Context context){
        if (instance == null){
            instance = new DataManager(context);
        }
        return instance;
    }

    private void initCharacters(Context context) {
        characters.clear();
        String[] names = context.getResources().getStringArray(R.array.character_names);
        String[] description = context.getResources().getStringArray(R.array.character_description);
        TypedArray cardIds = context.getResources().obtainTypedArray(R.array.characters_card_ids);
        int start = 0;
        if (twoPlayerGame) {
            start = 1;
        }
        for (int i = start; i < names.length; i++) {
            Character character = new Character(names[i], description[i], cardIds.getResourceId(i, -1));
            characters.add(character);
        }
    }

    public void addPlayer(Player player) {
        players.add(player);
    }

    public boolean hasMaxPlayers() {
        if (twoPlayerGame)
            return players.size() == 2;
        return players.size() == 4;
    }

    public boolean hasEnoughPlayers() {
        if (!twoPlayerGame) {
            return players.size() > 2;
        }
        return players.size() >= 2;
    }

    public void restart(Context context) {
        players.clear();
        initCharacters(context);
    }

    public List<Character> getCharacters() {
        return characters;
    }

    public void shuffleCharacters() {
        long seed = System.nanoTime();
        Collections.shuffle(characters, new Random(seed));
    }

    public Character getCharacterById(int characterId) {
        for (Character character : characters) {
            if (character.getId() == characterId) {
                return character;
            }
        }
        for (Player player : players) {
            if (player.getCharacter().getId() == characterId) {
                return player.getCharacter();
            }
        }
        return null;
    }

    public Player getPlayerById(int playerId) {
        for (Player player : players) {
            if (player.getId() == playerId) {
                return player;
            }
        }
        return null;
    }

    public void removeCharacter(Character character) {
        characters.remove(character);
    }

    public boolean hasPlayerWithName(String name) {
        for (Player player : players) {
            if (player.getName().contentEquals(name)) {
                return true;
            }
        }
        return false;
    }

    public List<Player> getPlayers() {
        return players;
    }

    public void addCharacter(Character character) {
        characters.add(character);
    }
}
