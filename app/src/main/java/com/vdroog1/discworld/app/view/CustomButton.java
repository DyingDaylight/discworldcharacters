package com.vdroog1.discworld.app.view;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.Button;
import com.vdroog1.discworld.app.R;

/**
 * Created by kettricken on 09.05.2015.
 */
public class CustomButton extends Button {

    public CustomButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public CustomButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CustomButton(Context context) {
        super(context);
        init();
    }

    public void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "font/BenguiatRegular.ttf");
        setTypeface(tf ,1);
        setTextColor(getResources().getColor(R.color.text_color));
    }
}
