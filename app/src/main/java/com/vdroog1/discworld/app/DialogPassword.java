package com.vdroog1.discworld.app;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.text.method.PasswordTransformationMethod;
import android.widget.LinearLayout;
import android.widget.Toast;
import com.vdroog1.discworld.app.view.CustomEditText;

/**
 * Created by kettricken on 09.05.2015.
 */
public class DialogPassword extends DialogFragment {

    public static final String TAG = DialogPassword.class.getSimpleName();

    public interface OnPasswordListener {
        public void onOk();
        public void onCancel();
    }

    OnPasswordListener listener;

    String password = "";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            password = savedInstanceState.getString("password");
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("password", password);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        MainActivity mainActivity = (MainActivity) activity;
        Fragment fragment = mainActivity.getCurrentFragment();
        listener = (OnPasswordListener) fragment;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        final CustomEditText input = new CustomEditText(getActivity());
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        input.setLayoutParams(lp);
        input.setTransformationMethod(new PasswordTransformationMethod());

        builder.setTitle(getString(R.string.game_password))
                .setMessage(getString(R.string.game_password_msg))
                .setCancelable(true)
                .setView(input)
                .setPositiveButton(getString(R.string.ok),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                String password = input.getText().toString();
                                if (DialogPassword.this.password.equals(password)) {
                                    listener.onOk();
                                } else {
                                    Toast.makeText(getActivity(), getString(R.string.game_password_wrong),
                                            Toast.LENGTH_SHORT).show();
                                }
                            }
                        })
                .setNegativeButton(getString(R.string.cancel),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                            }
                        });
        return builder.create();
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
