package com.vdroog1.discworld.app.data;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by kettricken on 09.05.2015.
 */
public class Character {

    private static final AtomicInteger count = new AtomicInteger(0);
    public static final String characterId = "characterId";

    private int id;
    private String name = "";
    private String description = "";
    private int cardId;

    public Character(String name, String description, int cardId) {
        id = count.getAndIncrement();
        this.name = name;
        this.description = description;
        this.cardId = cardId;
    }

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }

    public int getCardId() {
        return cardId;
    }

    public String getDescription() {
        return description;
    }
}
