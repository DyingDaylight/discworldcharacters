package com.vdroog1.discworld.app;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;
import com.vdroog1.discworld.app.data.DataManager;
import com.vdroog1.discworld.app.data.Character;
import com.vdroog1.discworld.app.data.Player;

/**
 * Created by kettricken on 09.05.2015.
 */
public class FragmentCharacterList extends Fragment {

    public static String TAG = FragmentCharacterList.class.getSimpleName();
    public static String STATE = "STATE";

    enum State { CHOOSE, RECHOOSE, WATCH }

    View rootView;

    State state;
    CharacterAdapter adapter;
    private DataManager dataManager;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        dataManager = DataManager.getInstance(getActivity().getBaseContext());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_characters_chooser, container, false);

        state = (State) getArguments().getSerializable(FragmentCharacterList.STATE);

        dataManager.shuffleCharacters();

        GridView gridView = (GridView) rootView.findViewById(R.id.character_chooser_grid);
        adapter = new CharacterAdapter(getActivity(), dataManager.getCharacters());
        if (state == State.WATCH) {
            adapter.setShowCharacters(true);
        } else {
            adapter.setShowCharacters(false);
        }
        gridView.setAdapter(adapter);
        gridView.setOnItemClickListener(onCardClickListener);
        return rootView;
    }

    AdapterView.OnItemClickListener onCardClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            Character character = dataManager.getCharacters().get(position);
            if (state == State.WATCH) {
                if (position == adapter.getExcludedPosition()) {
                    Toast.makeText(getActivity(), getString(R.string.character_watch_error), Toast.LENGTH_SHORT).show();
                } else {
                    showTarget(character);
                }
            } else {
                chooseCharacter(character);
            }
        }
    };

    private void showTarget(Character character) {
        DialogDescription dialogDescription = new DialogDescription();
        dialogDescription.setDescription(character.getDescription());
        dialogDescription.show(getActivity().getSupportFragmentManager(), DialogDescription.TAG);
    }

    private void chooseCharacter(Character character) {
        int playerId = getArguments().getInt(Player.playerId);
        int characterId = character.getId();

        openCharacterFragment(playerId, characterId);

        Toast.makeText(getActivity(), character.getName(), Toast.LENGTH_SHORT).show();
    }

    private void openCharacterFragment(int playerId, int characterId) {
        Bundle bundle = new Bundle();
        bundle.putInt(Player.playerId, playerId);
        bundle.putInt(Character.characterId, characterId);
        if (state == State.CHOOSE) {
            bundle.putSerializable(FragmentCharacter.STATE, FragmentCharacter.State.CHOOSE);
        } else if (state == State.RECHOOSE) {
            bundle.putSerializable(FragmentCharacter.STATE, FragmentCharacter.State.RECHOOSE);
        }
        Fragment fragment = new FragmentCharacter();
        fragment.setArguments(bundle);
        MainActivity mainActivity = (MainActivity) getActivity();
        mainActivity.changeFragment(fragment, FragmentCharacter.TAG);
    }
}
