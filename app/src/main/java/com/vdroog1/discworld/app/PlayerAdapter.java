package com.vdroog1.discworld.app;

import android.content.Context;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.vdroog1.discworld.app.data.Player;

import java.util.List;

/**
 * Created by kettricken on 09.05.2015.
 */
public class PlayerAdapter extends BaseAdapter {

    Context context;

    List<Player> players;

    float cardRatio = 0.64f;

    boolean isGameFinished = false;

    public PlayerAdapter(Context context, List<Player> players) {
        this.context = context;
        this.players = players;
    }

    private class ViewHolder {
        TextView nameTV;
        ImageView cardIV;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;

        if(convertView==null){
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.grid_item, parent, false);

            viewHolder = new ViewHolder();
            viewHolder.nameTV = (TextView) convertView.findViewById(R.id.grid_player_name);
            viewHolder.cardIV = (ImageView) convertView.findViewById(R.id.grid_player_card);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        Player player = players.get(position);

        if(player != null) {
            viewHolder.nameTV.setText(player.getName());
            if (isGameFinished) {
                viewHolder.cardIV.setImageResource(player.getCharacter().getCardId());
            } else {
                viewHolder.cardIV.setImageDrawable(context.getResources().getDrawable(R.drawable.back));
            }
            viewHolder.cardIV.setScaleType(ImageView.ScaleType.FIT_XY);

            DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
            float pxCardWidth = displayMetrics.widthPixels / 2;
            float pxCardHeight = pxCardWidth / cardRatio;

            viewHolder.cardIV.setLayoutParams(new LinearLayout.LayoutParams((int) pxCardWidth, (int) pxCardHeight));
        }

        return convertView;
    }


    public final int getCount() {
        return players.size();
    }

    public final Object getItem(int position) {
        return players.get(position % players.size());
    }

    public final long getItemId(int position) {
        return position;
    }

    public void setGameFinished() {
        isGameFinished = true;
    }
}
