package com.vdroog1.discworld.app;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;

/**
 * Created by kettricken on 09.05.2015.
 */
public class DialogConfirmation extends DialogFragment {

    public static final String TAG = DialogConfirmation.class.getSimpleName();

    public interface OnConfirmedListener {
        public void onConfirmed();
        public void onConfirmationCanceled();
    }

    OnConfirmedListener listener;
    String title;
    String message;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        MainActivity mainActivity = (MainActivity) activity;
        Fragment fragment = mainActivity.getCurrentFragment();
        listener = (OnConfirmedListener) fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            title = savedInstanceState.getString("title");
            message = savedInstanceState.getString("message");
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("title", title);
        outState.putString("message", message);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(title)
                .setMessage(message)
                .setCancelable(true)
                .setPositiveButton(getString(R.string.ok),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                listener.onConfirmed();
                            }
                        })
                .setNegativeButton(getString(R.string.cancel),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                listener.onConfirmationCanceled();
                            }
                        });
        return builder.create();
    }

    public void setTitle(String title){
        this.title = title;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
