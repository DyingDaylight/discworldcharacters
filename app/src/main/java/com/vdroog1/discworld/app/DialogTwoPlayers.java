package com.vdroog1.discworld.app;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;

/**
 * Created by kettricken on 09.05.2015.
 */
public class DialogTwoPlayers extends DialogFragment {

    public static final String TAG = DialogTwoPlayers.class.getSimpleName();

    public interface OnTwoPlayersListener {
        public void onTwoPlayers();
        public void onMorePlayers();
    }

    OnTwoPlayersListener listener;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        MainActivity mainActivity = (MainActivity) activity;
        listener = (OnTwoPlayersListener) mainActivity;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        setCancelable(false);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(getString(R.string.players_number))
                .setMessage(getString(R.string.players_number_msg))
                .setCancelable(false)
                .setPositiveButton(getString(R.string.players_two),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                                listener.onTwoPlayers();
                            }
                        })
                .setNegativeButton(getString(R.string.players_more),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                                listener.onMorePlayers();
                            }
                        });
        return builder.create();
    }
}
