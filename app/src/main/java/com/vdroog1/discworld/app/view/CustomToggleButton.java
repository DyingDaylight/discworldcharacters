package com.vdroog1.discworld.app.view;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.ToggleButton;
import com.vdroog1.discworld.app.R;

/**
 * Created by kettricken on 11.05.2015.
 */
public class CustomToggleButton extends ToggleButton {

    public CustomToggleButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public CustomToggleButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CustomToggleButton(Context context) {
        super(context);
        init();
    }

    public void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "font/BenguiatRegular.ttf");
        setTypeface(tf ,1);
        setTextColor(getResources().getColor(R.color.text_color));
    }
}
