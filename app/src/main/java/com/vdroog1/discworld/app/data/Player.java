package com.vdroog1.discworld.app.data;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by kettricken on 09.05.2015.
 */
public class Player {

    private static final AtomicInteger count = new AtomicInteger(0);
    public static final String playerId = "playerId";

    private int id;
    private String name = "";
    private String password = "";
    private Character character;

    public Player(String name, String password) {
        this.id = count.getAndIncrement();
        this.name = name;
        this.password = password;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setCharacter(Character character) {
        this.character = character;
    }

    public Character getCharacter() {
        return character;
    }

    public String getPassword() {
        return password;
    }
}
