package com.vdroog1.discworld.app;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;
import com.vdroog1.discworld.app.data.DataManager;
import com.vdroog1.discworld.app.data.Player;

/**
 * Created by kettricken on 09.05.2015.
 */
public class FragmentPlayers extends Fragment implements DialogConfirmation.OnConfirmedListener {

    public static String TAG = FragmentPlayers.class.getSimpleName();

    View rootView;

    DataManager dataManager;

    public FragmentPlayers() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        dataManager = DataManager.getInstance(getActivity().getBaseContext());

        if (dataManager.hasMaxPlayers()) {
            openGameFragment();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_players, container, false);

        rootView.findViewById(R.id.player_create_btn).setOnClickListener(onButtonClickListener);
        rootView.findViewById(R.id.player_finish_btn).setOnClickListener(onButtonClickListener);

        return rootView;
    }

    View.OnClickListener onButtonClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.player_create_btn:
                    EditText nameET = (EditText) rootView.findViewById(R.id.player_name);
                    EditText passwordET = (EditText) rootView.findViewById(R.id.player_password);

                    String name = nameET.getText().toString();
                    String password = passwordET.getText().toString();

                    if (validatePlayerCreation(name, password)){
                        Player player = new Player(name, password);
                        dataManager.addPlayer(player);
                        openCharacterChooser(player);
                    }
                    break;
                case R.id.player_finish_btn:
                    if (validatePlayersNumber()){
                        DialogConfirmation dialogConfirmation = new DialogConfirmation();
                        dialogConfirmation.setTitle(getString(R.string.players_continue));
                        dialogConfirmation.setMessage(getString(R.string.players_continue_msg));
                        dialogConfirmation.show(getActivity().getSupportFragmentManager(), DialogConfirmation.TAG);
                    }
                    break;
            }
        }
    };

    private void openCharacterChooser(Player player) {
        Bundle bundle = new Bundle();
        bundle.putInt(Player.playerId, player.getId());
        bundle.putSerializable(FragmentCharacterList.STATE, FragmentCharacterList.State.CHOOSE);
        Fragment fragment = new FragmentCharacterList();
        fragment.setArguments(bundle);
        MainActivity mainActivity = (MainActivity) getActivity();
        mainActivity.changeFragment(fragment, FragmentCharacterList.TAG);
    }

    private void openGameFragment() {
        Fragment fragment = new FragmentGame();
        MainActivity mainActivity = (MainActivity) getActivity();
        mainActivity.changeFragment(fragment, FragmentGame.TAG);
    }

    private boolean validatePlayersNumber() {
        if (dataManager.hasEnoughPlayers() == false) {
            Toast.makeText(getActivity(), getString(R.string.player_small_number), Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private boolean validatePlayerCreation(String name, String password) {
        if (name.isEmpty()) {
            Toast.makeText(getActivity(), getString(R.string.player_name_empty), Toast.LENGTH_SHORT).show();
            return false;
        }

        if (password.isEmpty()) {
            Toast.makeText(getActivity(), getString(R.string.player_password_empty), Toast.LENGTH_SHORT).show();
            return false;
        }

        if (dataManager.hasPlayerWithName(name)){
            Toast.makeText(getActivity(), getString(R.string.player_exists), Toast.LENGTH_SHORT).show();
            return false;
        }

        return true;
    }

    @Override
    public void onConfirmed() {
        openGameFragment();
    }

    @Override
    public void onConfirmationCanceled() {

    }
}
