package com.vdroog1.discworld.app;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import com.vdroog1.discworld.app.data.DataManager;


public class MainActivity extends ActionBarActivity implements DialogTwoPlayers.OnTwoPlayersListener {

    DataManager dataManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        dataManager = DataManager.getInstance(getBaseContext());
        if (dataManager.getPlayers().size() == 0) {
            askTwoPlayers();
        }

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, new FragmentPlayers(), FragmentPlayers.TAG)
                    .commit();
        }
    }

    private void askTwoPlayers() {
        DialogTwoPlayers twoPlayers = new DialogTwoPlayers();
        twoPlayers.show(getSupportFragmentManager(), DialogTwoPlayers.TAG);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_restart) {
            askTwoPlayers();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void changeFragment(Fragment emptyFragment, String tag) {
        changeFragment(emptyFragment, tag, false);
    }

    public Fragment getCurrentFragment() {
        return getSupportFragmentManager().findFragmentById(R.id.container);
    }

    public void changeFragment(Fragment emptyFragment, String tag, boolean addToBackStack) {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.container);
        if (fragment.equals(emptyFragment)) {
            return;
        } else {
            fragment = emptyFragment;
        }
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.container, fragment, tag);
        if (addToBackStack) {
            transaction.addToBackStack("");
        }
        transaction.commit();
    }

    @Override
    public void onTwoPlayers() {
        DataManager.twoPlayerGame = true;
        startSession();
    }

    @Override
    public void onMorePlayers() {
        DataManager.twoPlayerGame = false;
        startSession();
    }

    private void startSession() {
        dataManager = DataManager.getInstance(getBaseContext());
        dataManager.restart(getBaseContext());
        Fragment fragment = new FragmentPlayers();
        changeFragment(fragment, FragmentPlayers.TAG);
    }

    @Override
    public void onBackPressed() {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.container);
        if (fragment instanceof FragmentCharacter) {
            if (((FragmentCharacter) fragment).onBackPressed() == false) {
                super.onBackPressed();
            }
        } else {
            super.onBackPressed();
        }
    }

    public void restart() {
        askTwoPlayers();
    }
}
